package com.projettut.asuivre.tools;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.projettut.asuivre.controleur.Control;
import com.projettut.asuivre.model.AccessDistant;
import com.projettut.asuivre.view.Activity.CategoriesActivity;
import com.projettut.asuivre.view.Activity.CommentActivity;
import com.projettut.asuivre.view.Activity.CreateStoryActivity;
import com.projettut.asuivre.view.Activity.FullStoryDisplayActivity;
import com.projettut.asuivre.view.Activity.HomeActivity;
import com.projettut.asuivre.view.Activity.WelcomeActivity;
import com.projettut.asuivre.view.Fragment.CategoriesFragment;
import com.projettut.asuivre.view.Fragment.CommentFragmentDisplay;
import com.projettut.asuivre.view.Fragment.HomeFragment;
import com.projettut.asuivre.view.Fragment.PargraphsDisplayFragment;
import com.projettut.asuivre.view.ListAdapter.CategoryListAdapter;
import com.projettut.asuivre.view.ListAdapter.CommentListAdapter;
import com.projettut.asuivre.view.ListAdapter.ParagraphListAdapter;
import com.projettut.asuivre.view.ListAdapter.StoryListAdapter;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class AccessHttp extends AsyncTask<String, Integer, Long> {

    private ArrayList<NameValuePair> parameters;
    private String returnString = null;
    public AsyncResponse delegate = null;
    private Control control;

    private Activity activity;


    public AccessHttp(Activity activity) {
        this.activity = activity;
        this.parameters = new ArrayList<NameValuePair>();
    }

    /**
     * Ajout d'un parametre post
     * @param name
     * @param value
     */
    public void addParam(String name, String value){
        parameters.add(new BasicNameValuePair(name, value));

    }


    public void emptyParams(){
        parameters = new ArrayList<NameValuePair>();
    }

    /**
     * Connexion en tache de fond dans un thread séparé
     * @param strings
     * @return
     */
    @Override
    protected Long doInBackground(String... strings) {
        Log.d("Func->doInBackGround","doInBackGround");
        HttpClient connexionHttp = new DefaultHttpClient();
        HttpPost paramConnexion = new HttpPost(strings[0]);
        try {
            // encodage des parametres
            paramConnexion.setEntity(new UrlEncodedFormEntity(parameters));
            Log.d("conn->doInBackGround","doInBackGround");
            // connexion et envoie des parametres, attente de reponse
            HttpResponse response = connexionHttp.execute(paramConnexion);
            Log.d("exec->doInBackGround","doInBackGround");
            // transformation de la réponse
            returnString = EntityUtils.toString(response.getEntity());
            Log.d("response", returnString);

        } catch (UnsupportedEncodingException e) {
            Log.d("Encoding Error", e.toString());
        } catch (ClientProtocolException e) {
            Log.d("Protocol Error", e.toString());
        } catch (IOException e) {
            Log.d("I/O Error", e.toString());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Long result){
        Log.d("postexecute", "postexete");
        delegate.processFinished(returnString);
        AccessDistant accessDistant = (AccessDistant) delegate;
        if (activity != null)
        {
            Log.d("***", "onPostExecute: " + returnString);
            Log.d("***", "onPostExecute: " + activity.getClass().getName());
            switch (activity.getClass().getName())
            {
                case "com.projettut.asuivre.view.Activity.HomeActivity":
                    if (returnString.startsWith("selectLikesByStory"))
                    {
                        Log.d("***", "onPostExecute: SELECTLIKESBYSTORY");
                        control = Control.getInstance();
                        control.textViewLike.setText(String.valueOf(control.likeNum));
                        //storyListAdapter.likeList = accessDistant.control.getLikesList();
                        Log.d("***", "onPostExecute: " + control.likeNum);
                    }
                    else
                    {
                        HomeActivity homeActivity = (HomeActivity) this.activity;
                        HomeFragment homeFragment = (HomeFragment) homeActivity.fragment;
                        StoryListAdapter storyListAdapter = new StoryListAdapter(accessDistant.control.getStoriesList());
                        homeFragment.recyclerView.setAdapter(storyListAdapter);
                    }
                    break;
                case "com.projettut.asuivre.view.Activity.CategoriesActivity":
                    CategoriesActivity categoryActivity = (CategoriesActivity) this.activity;
                    CategoriesFragment categoriesFragment = (CategoriesFragment) categoryActivity.fragment;
                    CategoryListAdapter categoryListAdapter = new CategoryListAdapter(accessDistant.control.getCategoriesList());
                    categoriesFragment.recyclerView.setAdapter(categoryListAdapter);
                    categoriesFragment.categoriesList = accessDistant.control.getCategoriesList();
                    Log.d("***", "onPostExecute: " + accessDistant.control.getCategoriesList().get(0));
                    break;
                case "com.projettut.asuivre.view.Activity.CreateStoryActivity":
                    control = Control.getInstance();
                    String message = returnString.split("%")[1];
                    CreateStoryActivity createStoryActivity = (CreateStoryActivity) activity;
                    message = message.substring(1);
                    try {
                        JSONArray jsonArray = new JSONArray(message);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        int id = jsonObject.getInt("id");
                        Log.d("***", "onPostExecute: " + id);
                        control.insertParagraph(0,1, id, createStoryActivity.edit_text_create_story_paragraph.getText().toString(), 0);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case "com.projettut.asuivre.view.Activity.CommentActivity":
                    Log.d("***", "onPostExecute: WHATAREYOUHEREFOR?");
                    control = Control.getInstance();
                    CommentActivity commentActivity = (CommentActivity) this.activity;
                    if (returnString.startsWith("insertComment"))
                    {
                        control.selectCommentsByStory(Integer.parseInt(commentActivity.story_id));
                    }
                    else
                    {
                        CommentFragmentDisplay commentFragmentDisplay = (CommentFragmentDisplay) commentActivity.fragment;
                        CommentListAdapter commentListAdapter = new CommentListAdapter(control.getCommentsList());
                        commentFragmentDisplay.recyclerView.setAdapter(commentListAdapter);
                        commentFragmentDisplay.commentList = control.getCommentsList();
                    }
                    break;
                case "com.projettut.asuivre.view.Activity.FullStoryDisplayActivity":
                    control = Control.getInstance();
                    FullStoryDisplayActivity fullStoryDisplayActivity = (FullStoryDisplayActivity) this.activity;
                    ParagraphListAdapter paragraphListAdapter = new ParagraphListAdapter(control.getParagraphsList());
                    PargraphsDisplayFragment fragment = (PargraphsDisplayFragment) fullStoryDisplayActivity.pargraphsDisplayFragment;
                    fragment.recyclerView.setAdapter(paragraphListAdapter);
                    break;
                case "com.projettut.asuivre.view.Activity.WelcomeActivity":
                    if(returnString.startsWith("select")){
                        WelcomeActivity welcomeActivity = (WelcomeActivity) this.activity;
                        welcomeActivity.newUser();
                    }
                    break;
            }
        }
    }
}
