package com.projettut.asuivre.tools;

public interface AsyncResponse {
    void processFinished(String output);
}
