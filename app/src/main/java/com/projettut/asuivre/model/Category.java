package com.projettut.asuivre.model;

import org.json.JSONArray;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Category implements Serializable {

    // Variantes
    private int id;
    private String category_name;
    private int required_majority;


    public Category(int id, String category_name, int required_majority) {
        this.id = id;
        this.category_name = category_name;
        this.required_majority = required_majority;
    }




    public JSONArray convertToJSONArray(){
        List list = new ArrayList();
        list.add(id);
        list.add(category_name);
        list.add(required_majority);

        return new JSONArray(list);
    }

    public String getCategory_name() {
        return category_name;
    }

    public int getRequired_majority() {
        return required_majority;
    }

    public int getId() {
        return id;
    }
}
