package com.projettut.asuivre.model;

import android.app.Activity;
import android.util.Log;
import android.widget.TextView;

import com.projettut.asuivre.controleur.Control;
import com.projettut.asuivre.tools.AccessHttp;
import com.projettut.asuivre.tools.AsyncResponse;

import org.json.JSONArray;

import java.io.Serializable;

public class AccessDistant implements AsyncResponse, Serializable {

    private static final String SERVERADDR = "http://daafa3eb.ngrok.io/asuivre-serveur/server.php";
    public Control control;
    private Activity activity;
    public TextView textView = null;

    public AccessDistant(Control control, Activity activity)
    {
        this.control = control;
        this.activity = activity;
    }

    public AccessDistant(Control control, Activity activity, TextView textView)
    {
        this.control = control;
        this.activity = activity;
        this.textView = textView;
    }

    /**
     * retour du serveur distant
     * @param output
     */
    @Override
    public void processFinished(String output) {
        Log.d("server",output);
        // découpage du message reçu
        String[] message= output.split("%");
        // dans message[0] enreg dernier error
        // dans message[1] reste du message


        if (message.length ==1 && message[0].equals("selectStoriesByCategory")){
            control.setStoriesList(null);
        }
        if (message.length ==1 && message[0].equals("selectUserWithEmail")){
            Log.d("UserDoesntexist", "-------");
            control.setUserExists(false);
        }

        // si il y a deux cases
        if (message.length > 1){
            switch (message[0]){
            // --- USER ---
                case "insertUser":
                    Log.d("insertUser", message[1]);
                    break;
                case "selectUsers":
                    Log.d("selectUsers", message[1]);
                    control.recupUsers(message[1]);
                    break;
                case "selectUserWithEmail":
                    Log.d("User exists", "-------");
                    control.setUserExists(true);
                    control.recupUsers(message[1]);
                    break;
            // --- STORY ---
                case "insertStory":
                    Log.d("insertStory", message[1]);
                    break;
                case "selectStory":
                    control.recupStory(message[1]);
                    Log.d("selectStory", message[1]);
                    break;
                case "selectStoriesByCategory":
                    Log.d("selectStoriesByCat", message[1]);
                    control.recupStory(message[1]);
                    break;
                case "selectLikesByStory":
                    Log.d("selectLikesbyStory", message[1]);
                    control.recupLikesNum(message[1]);
                    break;
            // --- CATEGORY ---
                case "selectCategory":
                    Log.d("selectCategory", message[1]);
                    control.recupCategory(message[1]);
                    break;
            // PARAGRAPH
                case "insertParagraph":
                    Log.d("insertParagraph", message[1]);
                    break;
                case "selectParagraphsByStory":
                    Log.d("selectParagraph", message[1]);
                    control.recupParagraph(message[1]);
                    break;
            // COMMENT
                case "insertComment":
                    Log.d("insertComment", message[1]);
                    break;
                case "selectCommentsByStory":
                    Log.d("selectComment", message[1]);
                    control.recupComment(message[1]);
                    break;
            // LIKE
                case "insertLike":
                    Log.d("insertLike", message[1]);
                    break;
                case "selectLikes":
                    Log.d("selectLikes", message[1]);
                    control.recupLike(message[1]);
                    break;
            // ==== ERREUR ====
                case "Error !":
                    Log.d("Error !", message[1]);
            }
        }
        Log.d("ProcessFinishedEnd", "****************************");
        if (activity == null && message[0].startsWith("selectParagraph"))
        {
            Log.d("***", "processFinished: " + message[0]);
            control = Control.getInstance();
            this.textView.setText(control.getParagraphsList().get(0).getParagraph_text());
        }
    }

    public void send(String operation, JSONArray datasJSON){
        Log.d("debutSend", "debutSend");
        AccessHttp accessData = new AccessHttp(activity);
        // lien de délégation
        accessData.delegate = this;
        // ajout de parametres
        accessData.addParam("operation", operation);
        accessData.addParam("datas", datasJSON.toString());
        Log.d("data", datasJSON.toString());
        // Appel au serveur
        accessData.execute(SERVERADDR); //appelle le doInBackGround
        Log.d("finSend", "finSend");
    }
}
