package com.projettut.asuivre.model;

import org.json.JSONArray;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Paragraph implements Serializable {
    // Variantes
    private int id;
    private int user_id;
    private int story_id;
    private String paragraph_text;
    private int reportings;

    public Paragraph(int id, int user_id, int story_id, String paragraph_text, int reportings) {
        this.id = id;
        this.user_id = user_id;
        this.story_id = story_id;
        this.paragraph_text = paragraph_text;
        this.reportings = reportings;
    }


    public JSONArray convertToJSONArray(){
        List list = new ArrayList();
        list.add(id);
        list.add(user_id);
        list.add(story_id);
        list.add(paragraph_text);
        list.add(reportings);

        return new JSONArray(list);
    }

    public String getParagraph_text() {
        return paragraph_text;
    }

    public void setParagraph_text(String paragraph_text) {
        this.paragraph_text = paragraph_text;
    }

    public int getId() {
        return id;
    }
}
