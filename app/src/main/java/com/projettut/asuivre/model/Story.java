package com.projettut.asuivre.model;

import org.json.JSONArray;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Story implements Serializable {
    // constantes (conditions)



    // variables
    private int id;
    private int category_id;
    private int user_id;
    private String story_name;
    private String firstParagraph;
    private int likeNum;


    public Story(int id, int category_id, int user_id, String story_name) {
        this.id = id;
        this.category_id = category_id;
        this.user_id = user_id;
        this.story_name = story_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getStory_name() {
        return story_name;
    }

    public void setStory_name(String story_name) {
        this.story_name = story_name;
    }

    public String getFirstParagraph() {
        return firstParagraph;
    }

    public void setFirstParagraph(String firstParagraph) {
        this.firstParagraph = firstParagraph;
    }

    public int getLikeNum() {
        return likeNum;
    }

    public void setLikeNum(int likeNum) {
        this.likeNum = likeNum;
    }

    public JSONArray convertToJSONArray(){
        List<Serializable> list = new ArrayList<>();
        list.add(this.id);
        list.add(this.category_id);
        list.add(this.user_id);
        list.add(this.story_name);

        return new JSONArray(list);
    }
}
