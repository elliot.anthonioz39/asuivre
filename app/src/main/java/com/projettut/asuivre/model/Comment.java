package com.projettut.asuivre.model;

import org.json.JSONArray;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Comment implements Serializable {
    // Variantes
    private int id;
    private int user_id;
    private int story_id;
    private String commentary_text;
    private int report;


    public Comment(int id, int user_id, int story_id, String commentary_text, int report) {
        this.id = id;
        this.user_id = user_id;
        this.story_id = story_id;
        this.commentary_text = commentary_text;
        this.report = report;
    }


    public JSONArray convertToJSONArray(){
        List list = new ArrayList();
        list.add(id);
        list.add(user_id);
        list.add(story_id);
        list.add(commentary_text);
        list.add(report);

        return new JSONArray(list);
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getUser_id() {
        return user_id;
    }
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
    public int getStory_id() {
        return story_id;
    }
    public void setStory_id(int story_id) {
        this.story_id = story_id;
    }
    public String getCommentary_text() {
        return commentary_text;
    }
    public void setCommentary_text(String commentary_text) {
        this.commentary_text = commentary_text;
    }
    public int getReport() {
        return report;
    }
    public void setReport(int report) {
        this.report = report;
    }
}
