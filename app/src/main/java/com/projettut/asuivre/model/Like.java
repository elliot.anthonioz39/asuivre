package com.projettut.asuivre.model;

import org.json.JSONArray;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Like implements Serializable {
    // Constantes


    // Variantes
    private int id;
    private int story_id;
    private int user_id;


    public Like(int id, int story_id, int user_id) {
        this.id = id;
        this.story_id = story_id;
        this.user_id = user_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStory_id() {
        return story_id;
    }

    public void setStory_id(int story_id) {
        this.story_id = story_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }


    public JSONArray convertToJSONArray(){
        List<Serializable> list = new ArrayList<>();
        list.add(this.id);
        list.add(this.user_id);
        list.add(this.story_id);

        return new JSONArray(list);
    }
}
