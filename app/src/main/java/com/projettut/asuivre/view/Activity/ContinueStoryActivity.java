package com.projettut.asuivre.view.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.projettut.asuivre.controleur.Control;
import com.projettut.asuivre.model.Story;

import java.util.ArrayList;
import java.util.List;

import com.projettut.asuivre.R;

public class ContinueStoryActivity extends AppCompatActivity {

    private Control control = Control.getInstance();
    private List<Story> storyList = new ArrayList<>();
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_continue_story);

        final Story story = (Story) getIntent().getExtras().getSerializable("Story");
        TextView storyToContinue = findViewById(R.id.story_to_continue_text_view);
        final TextInputEditText newParagraph = findViewById(R.id.sequel_input);
        storyToContinue.setText(story.getFirstParagraph());

        BottomNavigationView navigation = findViewById(R.id.navigation_continue_story);
        mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        Intent i1 = new Intent(getBaseContext(), HomeActivity.class);
                        startActivity(i1);
                        break;
                    case R.id.navigation_profile:
                        Intent i3 = new Intent(getBaseContext(), UserActivity.class);
                        startActivity(i3);
                        break;
                    case R.id.navigation_categories:
                        Intent i2 = new Intent(getBaseContext(), CategoriesActivity.class);
                        startActivity(i2);
                        break;
                }
                return true;
            }
        };
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Button publish = findViewById(R.id.publish_sequel_button);
        publish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Control control = Control.getInstance();
                String paragraph = newParagraph.getText().toString();
                control.insertParagraph(0, 1, story.getId(), paragraph, 0);
                startActivity(new Intent(ContinueStoryActivity.this, HomeActivity.class));
            }
        });
    }
}
