package com.projettut.asuivre.view.Activity;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.projettut.asuivre.R;
import com.projettut.asuivre.controleur.Control;


public class CreateStoryActivity extends AppCompatActivity {

    private EditText edit_text_create_story_title;
    public EditText edit_text_create_story_paragraph;
    public Button button_create_story_publish;
    private Control control;
    private Button button;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_story);

        BottomNavigationView navigation = findViewById(R.id.navigation_create_story);
        mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        Intent i1 = new Intent(getBaseContext(), HomeActivity.class);
                        startActivity(i1);
                        break;
                    case R.id.navigation_profile:
                        Intent i3 = new Intent(getBaseContext(), UserActivity.class);
                        startActivity(i3);
                        break;
                    case R.id.navigation_categories:
                        Intent i2 = new Intent(getBaseContext(), CategoriesActivity.class);
                        startActivity(i2);
                        break;
                }
                return true;
            }
        };
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        control = Control.getInstance(); /*(Control) getIntent().getSerializableExtra("Control");*/
        control.setActivity(this);
        button_create_story_publish = findViewById(R.id.create_new_story_button);
        edit_text_create_story_title = (TextInputEditText) findViewById(R.id.create_new_story_title_input);
        edit_text_create_story_paragraph = (TextInputEditText) findViewById(R.id.create_new_story_paragraphe_input);

        button_create_story_publish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String new_story_name = edit_text_create_story_title.getText().toString();
                String content = edit_text_create_story_paragraph.getText().toString();
                Log.d("data",new_story_name + "  " + content);
                control = Control.getInstance();
                control.insertStory(0,1,1, new_story_name);
                Log.d("***", "onClick: " + control.getStory().getId());
                //control.insertParagraph(0,1, control.getStory().getId(),content, 0);
                //control.insertComment(0,1,1,"comment",0);
                startActivity(new Intent(CreateStoryActivity.this, HomeActivity.class));

            }
        });
    }







}
