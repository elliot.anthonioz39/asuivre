package com.projettut.asuivre.view.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.projettut.asuivre.R;

import org.w3c.dom.Text;

public class StoryViewHolder extends RecyclerView.ViewHolder {

    public TextView textViewHistoire, textViewLike, textViewId, textViewTitle;
    public CheckBox checkBoxLike, checkBoxFavs;
    public Button buttonComment, buttonShare;

    public StoryViewHolder(@NonNull View itemView) {
        super(itemView);
        textViewHistoire = (TextView) itemView.findViewById(R.id.story);
        checkBoxLike = (CheckBox) itemView.findViewById(R.id.like);
        textViewLike = (TextView) itemView.findViewById(R.id.like_number);
        textViewTitle = (TextView) itemView.findViewById(R.id.story_title);
        buttonComment = (Button) itemView.findViewById(R.id.comment);
        buttonShare = (Button) itemView.findViewById(R.id.share);
        textViewId = (TextView) itemView.findViewById(R.id.story_id_text_view);
    }

    public Button getButtonShare() {
        return buttonShare;
    }
}
