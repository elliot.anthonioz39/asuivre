package com.projettut.asuivre.view.Activity;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.projettut.asuivre.R;
import com.projettut.asuivre.controleur.Control;
import com.projettut.asuivre.view.Fragment.CategoriesFragment;

public class CategoryActivity extends AppCompatActivity implements CategoriesFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        Control control = (Control) getIntent().getSerializableExtra("Control");
        CategoriesFragment categoriesFragment = new CategoriesFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("Control", control);
        categoriesFragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.category_recycler_view, categoriesFragment).commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
