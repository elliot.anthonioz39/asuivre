package com.projettut.asuivre.view.ListAdapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.projettut.asuivre.controleur.Control;
import com.projettut.asuivre.view.ViewHolder.CommentViewHolder;
import com.projettut.asuivre.R;
import com.projettut.asuivre.model.Comment;

import java.util.List;

public class CommentListAdapter extends RecyclerView.Adapter<CommentViewHolder> {

    List<Comment> commentsList;

    public CommentListAdapter(List <Comment> comments){
        this.commentsList = comments;
    }

    @NonNull
    @Override
    public CommentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_comment_display, viewGroup,false );
        return new CommentViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final CommentViewHolder commentViewHolder, int i) {
        final Comment comment = commentsList.get(i);
        final Control control = Control.getInstance();
        commentViewHolder.userName.setText(Integer.toString(comment.getUser_id()));
        commentViewHolder.commentText.setText(comment.getCommentary_text());
        commentViewHolder.reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String toastMessage = "Successfully reported comment!";
                Toast.makeText(control.activity, toastMessage, Toast.LENGTH_SHORT).show();
                control.reportComment(comment.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return commentsList.size();
    }
}
