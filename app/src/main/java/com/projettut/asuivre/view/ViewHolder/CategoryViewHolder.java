package com.projettut.asuivre.view.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.projettut.asuivre.R;

public class CategoryViewHolder extends RecyclerView.ViewHolder {
    public TextView categoryTextView;

    public CategoryViewHolder(View itemView) {
        super(itemView);
        categoryTextView = (TextView) itemView.findViewById(R.id.text_view_category);
    }
}
