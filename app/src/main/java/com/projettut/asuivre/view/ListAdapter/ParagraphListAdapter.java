package com.projettut.asuivre.view.ListAdapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.projettut.asuivre.R;
import com.projettut.asuivre.controleur.Control;
import com.projettut.asuivre.model.Paragraph;
import com.projettut.asuivre.view.ViewHolder.ParagraphViewHolder;

import java.util.List;

public class ParagraphListAdapter extends RecyclerView.Adapter<ParagraphViewHolder> {

    public List<Paragraph> paragraphList;
    private Button share, reportButton;
    private TextView text;
    private Control control;

    public ParagraphListAdapter(List<Paragraph> paragraphList) {
        this.paragraphList = paragraphList;
        //Log.d("*****", "ParagraphListAdapter: " + paragraphList.get(0).getParagraph_text());
    }

    @NonNull
    @Override
    public ParagraphViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_full_paragraph_display, viewGroup, false);
        return new ParagraphViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ParagraphViewHolder paragraphViewHolder, int i) {
        final Paragraph paragraph = paragraphList.get(i);
        control = Control.getInstance();
        paragraphViewHolder.paragraphTextView.setText(paragraph.getParagraph_text());
        Log.d("***", "onBindViewHolder: " + paragraph.getParagraph_text());
        reportButton = paragraphViewHolder.reportButton;
        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String toastMessage = "Successfully reported paragraph!";
                Toast.makeText(control.activity, toastMessage, Toast.LENGTH_SHORT).show();
                control.reportParagraph(paragraph.getId());
            }
        });

    }

    @Override
    public int getItemCount() {
        return paragraphList.size();
    }
}
