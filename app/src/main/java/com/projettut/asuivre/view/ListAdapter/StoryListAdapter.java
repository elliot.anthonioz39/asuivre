package com.projettut.asuivre.view.ListAdapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.CheckBox;
import android.widget.Toast;

import com.projettut.asuivre.model.Like;
import com.projettut.asuivre.view.Activity.FullStoryDisplayActivity;
import com.projettut.asuivre.R;
import com.projettut.asuivre.controleur.Control;
import com.projettut.asuivre.model.Story;
import com.projettut.asuivre.view.Activity.CommentActivity;
import com.projettut.asuivre.view.Activity.ContinueStoryActivity;
import com.projettut.asuivre.view.Activity.HomeActivity;
import com.projettut.asuivre.view.ViewHolder.StoryViewHolder;

import java.util.ArrayList;
import java.util.List;

public class StoryListAdapter extends RecyclerView.Adapter<StoryViewHolder> {


    private Button share;
    private TextView text;
    private Control control = Control.getInstance();
    private List<Story> storyList;
    public List<Like> likeList = new ArrayList<>();
    private CheckBox checkBoxLike;
    private Story story;
    public TextView textViewLike, textViewContinueStory, textViewTitle;

    private Button commentButton;

    public StoryListAdapter(List<Story> storyList) {
        this.storyList = storyList;
    }

    @NonNull
    @Override
    public StoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_story_display, viewGroup, false);
        return new StoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final StoryViewHolder storyViewHolder, int i) {
        story = storyList.get(i);

        control = Control.getInstance();

        storyViewHolder.textViewHistoire.setText(story.getStory_name());
        storyViewHolder.textViewId.setText(String.valueOf(story.getId()));

        // fonction lire histoire complete
        text = storyViewHolder.textViewHistoire;
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(storyViewHolder.itemView.getContext(), FullStoryDisplayActivity.class);
                intent.putExtra("Story", storyViewHolder.textViewId.getText().toString());
                storyViewHolder.itemView.getContext().startActivity(intent);
            }
        });

        // fonction share
        this.share = storyViewHolder.getButtonShare();
        control = Control.getInstance();
        final Control finalControl = control;
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareCompat.IntentBuilder
                        .from(finalControl.shareActivity/*finalControl.activity*/)
                        .setText("Cool story from asuivre : \n" + story.getFirstParagraph())
                        .setType("text/plain")
                        .setChooserTitle("Share this story")
                        .startChooser();
            }
        });


        checkBoxLike = storyViewHolder.itemView.findViewById(R.id.like);
        textViewLike = storyViewHolder.textViewLike;//.itemView.findViewById(R.id.like_number);
        textViewLike = storyViewHolder.itemView.findViewById(R.id.like_number);
        textViewTitle = storyViewHolder.itemView.findViewById(R.id.story_title);
        textViewContinueStory = storyViewHolder.itemView.findViewById(R.id.continue_story);
        commentButton = storyViewHolder.itemView.findViewById(R.id.comment);

        // Clique sur like récupérer l'id de la story , l'id de la ListAdapter
        checkBoxLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Snackbar snackbar = Snackbar.make(storyViewHolder.itemView.getRootView(), "Position in view " + storyViewHolder.getAdapterPosition() + " id story: " + story.getId(),Snackbar.LENGTH_SHORT);
                View view = snackbar.getView();
                TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                tv.setTextColor(Color.RED);
                snackbar.show();*/
                control.insertLike(0, 1, story.getId());
                String toastMessage = "Liked!";
                Toast.makeText(storyViewHolder.itemView.getContext(), toastMessage, Toast.LENGTH_SHORT).show();
            }
        });

        control.setTextViewLike(textViewLike);
        control.selectLikesByStory(story.getId());

        // Clique sur continue, pour ajouter un paragraphe à un
        textViewContinueStory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                control.selectParagraphsByStory(Integer.parseInt(storyViewHolder.textViewId.getText().toString()));
                Intent intent = new Intent(storyViewHolder.itemView.getContext(), ContinueStoryActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("Story", story);
                intent.putExtras(bundle);
                storyViewHolder.itemView.getContext().startActivity(intent);
            }
        });
        commentButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(storyViewHolder.itemView.getContext(), CommentActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("Story", storyViewHolder.textViewId.getText().toString());
                intent.putExtras(bundle);
                control.setActivity(null);
                storyViewHolder.itemView.getContext().startActivity(intent);

            }
        });
        control.setActivity(null);

        storyViewHolder.textViewHistoire.setText(story.getFirstParagraph());
        storyViewHolder.textViewTitle.setText(story.getStory_name());

    }

    @Override
    public int getItemCount() {
        return storyList.size();
    }
}
