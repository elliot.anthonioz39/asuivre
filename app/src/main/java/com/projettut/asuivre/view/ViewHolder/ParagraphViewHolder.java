package com.projettut.asuivre.view.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.projettut.asuivre.R;

public class ParagraphViewHolder extends RecyclerView.ViewHolder {

    public TextView paragraphTextView;
    public Button reportButton;

    public ParagraphViewHolder(@NonNull View itemView) {
        super(itemView);
        paragraphTextView = itemView.findViewById(R.id.story_display_paragraph);
        reportButton = itemView.findViewById(R.id.button_report_paragraph);
    }
}
